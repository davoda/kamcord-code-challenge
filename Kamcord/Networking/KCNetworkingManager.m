//
//  KCNetworkingManager.m
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCNetworkingManager.h"
#import "AFNetworking.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AFHTTPRequestOperationManager+KCKamcord.h"

#define kKamCordHostAddress @"https://www.kamcord.com/app/v2"

@implementation KCNetworkingManager

+ (void)networkRequestWithMethod:(KCHTTPMethod)method
                         andPath:(NSString*)path
               andBodyParameters:(NSDictionary *)parameters
                       onSuccess:(KCNetworkRequestSuccess)success
                       onFailure:(KCNetworkRequestFailure)failure{
    
    [KCNetworkingManager networkRequestWithMethod:method andPath:path andBodyParameters:parameters andURLParameters:nil onSuccess:success onFailure:failure];
    
}

+ (void)networkRequestWithMethod:(KCHTTPMethod)method
                         andPath:(NSString *)path
               andBodyParameters:(NSDictionary *)parameters
                andURLParameters:(NSDictionary *)URLParamteters
                       onSuccess:(KCNetworkRequestSuccess)success onFailure:(KCNetworkRequestFailure)failure{
    
    NSString *URLString = [KCNetworkingManager urlWithPath:path];
    NSString *HTTPMethod = [KCNetworkingManager httpMethodForMethod:method];
    
    AFJSONRequestSerializer *serializerRequest = [AFJSONRequestSerializer serializer];
    [serializerRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializerRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    AFJSONResponseSerializer *serializerResponse = [AFJSONResponseSerializer serializer];
    serializerResponse.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    serializerResponse.readingOptions = NSJSONReadingAllowFragments;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = serializerRequest;
    manager.responseSerializer = serializerResponse;
    
    if (URLParamteters) {
        URLString = [KCNetworkingManager appendParameters:URLParamteters toURLString:URLString];
    }
    
    [manager HTTPMethod:HTTPMethod URLString:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success) {
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                if (responseObject[@"response"]) {
                    success(responseObject[@"response"]);
                }else{
                    [SVProgressHUD showErrorWithStatus:@"Opppps well this is awkward... We didn't get a response back here, I'm sure this will be patched right up!"];
                }
                
            }else{
                
                [SVProgressHUD showErrorWithStatus:@"Opppps well this is awkward... You're not suppose to see me!"];
                failure(nil);
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure){
            failure(error);
        }
        
        // Tempurary I have access to error response structure ;)
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        
    }];
    
}


//  LOCAL HELPERS
+ (NSString *)toString:(id)object{
    return [NSString stringWithFormat: @"%@", object];
}
+ (NSString *)urlEncode:(id)object{
    NSString *string = [self toString:object];
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}

+ (NSString *)appendParameters:(NSDictionary *)parameters toURLString:(NSString *)urlString{
    
    urlString = [urlString stringByAppendingString:@"?"];
    
    NSMutableArray *parts = [NSMutableArray array];
    for (id key in parameters) {
        id value = [parameters objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@=%@", [KCNetworkingManager urlEncode:key], [KCNetworkingManager urlEncode:value]];
        [parts addObject: part];
    }
    
    NSString *addition = [parts componentsJoinedByString: @"&"];
    NSString *finalURL = [urlString stringByAppendingString:addition];
    
    return finalURL;
}
+ (NSString*)urlWithPath:(NSString*)path{
    if (![[path substringToIndex:1] isEqualToString:@"/"]) {
        path = [NSString stringWithFormat:@"/%@", path];
    }
    
    NSString *requestURLString = [NSString stringWithFormat:@"%@%@", kKamCordHostAddress, path];
    return requestURLString;
}
+ (NSString*)httpMethodForMethod:(KCHTTPMethod)method{
    
    NSString *methodString;
    
    switch (method) {
        case KCHTTPMethodGET:
            methodString =  @"GET";
            break;
        case KCHTTPMethodPOST:
            methodString = @"POST";
            break;
        case KCHTTPMethodDELETE:
            methodString = @"DELETE";
            break;
    }
    
    return methodString;
}

@end
