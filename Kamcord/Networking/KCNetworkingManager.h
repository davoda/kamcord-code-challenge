//
//  KCNetworkingManager.h
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCModel.h"

typedef enum : NSUInteger {
    KCHTTPMethodGET,
    KCHTTPMethodPOST,
    KCHTTPMethodDELETE
} KCHTTPMethod;

typedef void(^KCNetworkRequestFailure)(NSError *error);
typedef void(^KCNetworkRequestSuccess)(NSDictionary *response);

@interface KCNetworkingManager : KCModel

+ (void)networkRequestWithMethod:(KCHTTPMethod)method
                         andPath:(NSString *)path
               andBodyParameters:(NSDictionary *)parameters
                       onSuccess:(KCNetworkRequestSuccess)success onFailure:(KCNetworkRequestFailure)failure;

+ (void)networkRequestWithMethod:(KCHTTPMethod)method
                         andPath:(NSString *)path
               andBodyParameters:(NSDictionary *)parameters
                andURLParameters:(NSDictionary *)URLParamteters
                       onSuccess:(KCNetworkRequestSuccess)success onFailure:(KCNetworkRequestFailure)failure;

@end
