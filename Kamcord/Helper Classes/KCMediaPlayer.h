//
//  KCMediaPlayer.h
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface KCMediaPlayer : NSObject

+ (instancetype)sharedInstance;
- (void)playMoviewWithURL:(NSURL *)url inView:(UIView *)view;

@end
