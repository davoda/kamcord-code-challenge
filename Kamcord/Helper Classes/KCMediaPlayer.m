//
//  KCMediaPlayer.m
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCMediaPlayer.h"
#import <MediaPlayer/MediaPlayer.h>

@interface KCMediaPlayer ()

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;

@end

@implementation KCMediaPlayer

+ (instancetype)sharedInstance{
    static KCMediaPlayer *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [KCMediaPlayer new];
    });
    return sharedInstance;
}

- (void)playMoviewWithURL:(NSURL *)url inView:(UIView *)view{
    
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
    self.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
    [self.moviePlayer prepareToPlay];
    [self.moviePlayer setShouldAutoplay:YES];
    
    // Set Frame.
    [self.moviePlayer.view setFrame:view.bounds];
    [view addSubview:self.moviePlayer.view];
    
    
    [self.moviePlayer setFullscreen:YES animated:YES];
    [self.moviePlayer play];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerDidExitFullScreen) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerDidExitFullScreen) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
}

- (void)playerDidExitFullScreen{
    
    [self.moviePlayer.view removeFromSuperview];
    self.moviePlayer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
