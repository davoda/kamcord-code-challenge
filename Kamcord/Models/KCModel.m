//
//  KCModel.m
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCModel.h"

@interface KCModel ()

+ (instancetype)modelWithCleanDetails:(NSDictionary *)details;

@end

@implementation KCModel

+ (NSArray *)modelsWithArray:(NSArray *)objects{
    
    NSMutableArray *modelsArray = [NSMutableArray new];
    [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [modelsArray addObject:[[self class] modelWithDetails:obj]];
        }
        
    }];
    return modelsArray;
    
}
+ (instancetype)modelWithDetails:(NSDictionary *)details{
    
    if (!details) {
        return nil;
    }
    
    NSMutableDictionary *cleanDetails = [details mutableCopy];
    for (NSString *key in cleanDetails.allKeys){
        
        if ([cleanDetails[key] isKindOfClass:[NSString class]]) {
            if ([cleanDetails[key] isEqualToString:@"<null>"]) {
                [cleanDetails removeObjectForKey:key];
            }
        }else if ([cleanDetails[key] isKindOfClass:[NSNull class]]){
            [cleanDetails removeObjectForKey:key];
        }
        
    }
    
    return [self modelWithCleanDetails:cleanDetails];
}
+ (instancetype)modelWithCleanDetails:(NSDictionary *)details{
    //Model subclass will overide this method.
    return nil;
}


@end
