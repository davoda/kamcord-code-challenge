//
//  KCModel.h
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KCModel : NSObject

@property (readonly, strong, nonatomic) NSString *objectId;
+ (NSArray *)modelsWithArray:(NSArray *)objects;
+ (instancetype)modelWithDetails:(NSDictionary *)details;

@end
