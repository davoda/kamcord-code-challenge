//
//  KCVideo.h
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCModel.h"

@interface KCVideo : KCModel

@property (readonly, nonatomic, strong) NSString *title;

@property (readonly, nonatomic, strong) NSURL *videoUrl;
@property (readonly, nonatomic, strong) NSURL *thumbnailUrl;

@end
