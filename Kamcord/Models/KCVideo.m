//
//  KCVideo.m
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCVideo.h"

@interface KCVideo ()

@property (readwrite, strong, nonatomic) NSString *objectId;
@property (readwrite, strong, nonatomic) NSString *title;
@property (readwrite, strong, nonatomic) NSURL *videoUrl;
@property (readwrite, strong, nonatomic) NSURL *thumbnailUrl;

@end

@implementation KCVideo
@synthesize objectId = _objectId;

+ (instancetype)modelWithCleanDetails:(NSDictionary *)cleanDetails{
    
    KCVideo *video = [KCVideo new];
    
    video.objectId = cleanDetails[@"video_id"]   ?   cleanDetails[@"video_id"]    :   nil;
    video.title = cleanDetails[@"title"]    ?   cleanDetails[@"title"]  :   nil;
    video.videoUrl = cleanDetails[@"video_url"] ? [NSURL URLWithString:cleanDetails[@"video_url"]]  : nil;
    
    if (cleanDetails[@"thumbnails"]) {
    
        id thumbnails = cleanDetails[@"thumbnails"];
        if ([thumbnails isKindOfClass:[NSDictionary class]]) {
            if (thumbnails[@"REGULAR"]) {
                video.thumbnailUrl = [NSURL URLWithString:thumbnails[@"REGULAR"]];
            }
        }
        
    }

    return video;
}

@end
