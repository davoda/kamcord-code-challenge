//
//  KCWatchTableViewCell.m
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCWatchTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface KCWatchTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundColorView;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (strong, nonatomic) SDWebImageManager *imageManager;

@end

@implementation KCWatchTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.imageManager = [SDWebImageManager sharedManager];
}

- (void)prepareForReuse{
    
    // Reset image before reuse.
    self.thumbImageView.image = nil;
    self.titleLabel.text = @"";
    
}

- (void)layoutSubviews{
    
    [self layoutIfNeeded];
    self.backgroundColorView.layer.masksToBounds = YES;
    self.backgroundColorView.layer.cornerRadius = self.backgroundColorView.frame.size.height/2;
    
}

- (void)setCurrentVideo:(KCVideo *)currentVideo{
    _currentVideo = currentVideo;
    
    // Reset image before reuse.
    self.thumbImageView.image = nil;
    self.titleLabel.text = _currentVideo.title;
    [self bringSubviewToFront:self.backgroundColorView.superview];
    
    [self layoutSubviews];
    
    if (!_currentVideo.thumbnailUrl) {
        return;
    }
    
    
    // I'd normally create a helper class for fetching images but I'm only using that here, and don't have much time left :(
    [self.imageManager downloadImageWithURL:_currentVideo.thumbnailUrl
                                    options:SDWebImageHighPriority
                                   progress:nil
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                      
                                      // Return if the cell has been reused and image doesn't belong to this video.
                                      if (![imageURL isEqual:_currentVideo.thumbnailUrl]){
                                          return;
                                      }
                                      
                                      // Only animate if image was not in cache
                                      CGFloat animationDuration = cacheType == SDImageCacheTypeNone? 0.2f : 0.0f;
                                      [UIView transitionWithView:self
                                                        duration:animationDuration
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{
                                                          self.thumbImageView.image = image;
                                                      } completion:NULL];
                                      
                                      
                                  }];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    
}

@end
