//
//  KCWatchTableViewCell.h
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KCVideo.h"

@interface KCWatchTableViewCell : UITableViewCell

@property (weak, nonatomic) KCVideo *currentVideo;

@end
