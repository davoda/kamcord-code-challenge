//
//  KCApi.h
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^KCFailureResultingBlock)(NSError *error);
typedef void (^KCVideosArrayResultingBlock)(NSArray *videos);

@interface KCAPI : NSObject

+ (void)fetchFeedVideosForPage:(NSInteger)page onSuccess:(KCVideosArrayResultingBlock)success onFailure:(KCFailureResultingBlock)failure;

@end
