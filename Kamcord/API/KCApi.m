//
//  KCApi.m
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCAPI.h"
#import "KCNetworkingManager.h"
#import "KCVideo.h"

@implementation KCAPI

+ (void)fetchFeedVideosForPage:(NSInteger)page onSuccess:(KCVideosArrayResultingBlock)success onFailure:(KCFailureResultingBlock)failure{
    
    NSString *path = @"videos/feed/";
    NSDictionary *urlParameters = @{
                                    @"feed_id": @0,
                                    @"page": [NSNumber numberWithInteger:page]
                                    };
    
    [KCNetworkingManager networkRequestWithMethod:KCHTTPMethodGET andPath:path andBodyParameters:nil andURLParameters:urlParameters onSuccess:^(NSDictionary *response) {
        
        
        if (response[@"video_list"]) {
            id videoList = response[@"video_list"];
            if ([videoList isKindOfClass:[NSArray class]]) {
                
                if (success) {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        NSArray *videos = [KCVideo modelsWithArray:videoList];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            success(videos);
                        });
                        
                    });
                    
                }
                return;
                
            }
        }
        
        failure(nil);
        
    } onFailure:failure];
    
    
}

@end
