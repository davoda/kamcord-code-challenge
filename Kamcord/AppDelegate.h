//
//  AppDelegate.h
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

