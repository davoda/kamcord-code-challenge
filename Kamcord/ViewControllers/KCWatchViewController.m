//
//  KCWatchViewController.m
//  Kamcord
//
//  Created by David Elsonbaty on 2015-04-06.
//  Copyright (c) 2015 David Elsonbaty. All rights reserved.
//

#import "KCWatchViewController.h"
#import "KCWatchTableViewCell.h"

#import "KCMediaPlayer.h"
#import "KCAPI.h"

#define kCustomCellHeight 175.0f
#define kCustomCellReuseIdentifier @"KCWatchTableViewCell"

@interface KCWatchViewController () <UITableViewDelegate, UITableViewDataSource>

@property (assign, nonatomic) BOOL isLoadingVideos;
@property (assign, nonatomic) NSInteger pageNumber;
@property (strong, nonatomic) NSMutableArray *videosListArray;

@property (weak, nonatomic) IBOutlet UITableView *videosTableView;

@end

@implementation KCWatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.videosTableView registerNib:[UINib nibWithNibName:kCustomCellReuseIdentifier bundle:nil] forCellReuseIdentifier:kCustomCellReuseIdentifier];
    
    [self fetchVideosListFromHead:YES];
    
}
- (void)fetchVideosListFromHead:(BOOL)fromHead{
    
    // Lock networking calls
    if (_isLoadingVideos) {
        return;
    }
    
    _isLoadingVideos = YES;
    if (fromHead) {
        self.pageNumber = 0;
    }else{
        self.pageNumber ++;
    }
    
    [KCAPI fetchFeedVideosForPage:self.pageNumber onSuccess:^(NSArray *videos) {
        
        if (self.pageNumber == 0) {
            self.videosListArray = [NSMutableArray new];
        }
        
        // Add new videos to the main videos array
        [self.videosListArray addObjectsFromArray:videos];
        
        // Animate videos array change in tableview
        if (self.pageNumber == 0) {
            
            [self.videosTableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.videosTableView.numberOfSections)] withRowAnimation:UITableViewRowAnimationFade];
            
        }else{
            
            [self.videosTableView beginUpdates];
            
            NSMutableArray *indexPaths = [NSMutableArray new];
            NSInteger oldVideosArrayCount = _videosListArray.count - videos.count;
            
            [videos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [indexPaths addObject:[NSIndexPath indexPathForItem:oldVideosArrayCount + idx inSection:0]];
            }];
            
            [self.videosTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationLeft];
            [self.videosTableView endUpdates];
            
        }
        
        // Unlock networking calls
        _isLoadingVideos = NO;
        
    } onFailure:^(NSError *error) {
        
        // Put back the page number if it was bumped
        if (!fromHead) {
            self.pageNumber --;
        }
        
        // Unlock networking calls
        _isLoadingVideos = NO;
        
    }];
    
}

#pragma mark - UITableViewDatasource Implementation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _videosListArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == _videosListArray.count - 3) {
        [self fetchVideosListFromHead:NO];
    }
    
    KCWatchTableViewCell *cell = [self.videosTableView dequeueReusableCellWithIdentifier:kCustomCellReuseIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Make sure the array contains an object at the current index... just incase
    if (self.videosListArray.count > indexPath.row) {
        cell.currentVideo = self.videosListArray[indexPath.row];
    }
    
    return cell;
}

#pragma mark - UITableViewDeletage Implementation
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kCustomCellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:YES];
    
    KCVideo *video = [self.videosListArray objectAtIndex:indexPath.row];
    
    // If no url found then dismiss playing movie.
    if (!video.videoUrl) {
        return;
    }
    
    // Play the movie of the current cell
    [[KCMediaPlayer sharedInstance] playMoviewWithURL:video.videoUrl inView:self.navigationController.view];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

@end
